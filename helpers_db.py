#----------LIBRARIES--------------#
from pymongo import MongoClient, errors
from dotenv import load_dotenv
import datetime
import base64
import gridfs
import os
import re
load_dotenv()



DOMAIN = os.getenv("DOMAIN")
PORT = os.getenv("PORT")
USERNAME = os.getenv("USERNAME")
PASSWORD = os.getenv("PASSWORD")
CLIENT = MongoClient(
        host = [ str(DOMAIN) + ":" + str(PORT) ],
        serverSelectionTimeoutMS = 3000, # 3 second timeoutdocker
        username = USERNAME,
        password = PASSWORD,
    )
DB = CLIENT["DB"]
CONFIG_COL = DB["CONFIG_COL"]
INPUT_IMAGES_COL = DB["INPUT_IMAGE_COL"]
OUTPUT_IMAGES_COL = DB["OUTPUT_IMAGE_COL"]
FS = gridfs.GridFS(DB, "WEIGHTS")

def add_file(file, file_type="input_image", file_name=None, prediction=None):

    if file_name == None:
        name_pattern = re.compile(r"^.*\/(.*)\..*$")
        file_name = re.findall(name_pattern, string=file)[0]

    with open(file, "rb") as binary_file:
        binary_file_data = binary_file.read()
        base64_encoded_file_data = base64.b64encode(binary_file_data)
        base64_file_message = base64_encoded_file_data.decode("utf-8")

    if file_type=="config":
        data = {
                "date": datetime.datetime.now(),
                "file_name" : file_name,
                "configuration_file" : base64_file_message
                }

        CONFIG_COL.insert_one(data)

    if file_type=="weights":
        with open(file, 'rb') as weights :
            FS.put(data=weights, filename=file_name)

    if file_type=="input_image":
        
        images = INPUT_IMAGES_COL.count_documents({"image_name" : file_name})

        if  images == 0:
            data = {
                'image_name' : file_name,
                'raw_image' : base64_file_message
                }
        else:
            find_one_and_update({"file_name": file_name}, 
                                {"$set": {
                                        "transformed_image":base64_file_message 
                                            }
                                })

        data = {
                'image_name' : file_name,
                'raw_image' : base64_file_message,
                'transformed_image' : base64_transformed_image_message
                }

        INPUT_IMAGES_COL.insert_one(data)

    if file_type=="output_image":
        data = {
                "image_name" : file_name,
                "image" : base64_file_message,
                "prediction" : prediction
                }

        OUTPUT_IMAGES_COL.insert_one(data)

def extract_file(file_name, file_type="config"):
    
    if file_type=="config" :
        data = CONFIG_COL.find(
                               {'file_name':file_name},
                               {"_id":1, "file_name":1, "configuration_file":1}
                               )[0]
        
        file_name = data["file_name"] + ".cfg"
        base64_file_bytes = data["configuration_file"].encode("utf-8")
        with open(file_name, "wb") as file_to_extract:
            decoded_file_data = base64.decodebytes(base64_file_bytes)
            file_to_extract.write(decoded_file_data)
        
        return file_name

    if file_type=="weights":
        req = DB.WEIGHTS.files.find({'filename':file_name})
        uid = req[0]['_id']
        file_name = req[0]['filename'] + '.weights'
        data =  FS.get(uid).read()
        with open(file_name, 'wb') as file_to_extract:
            file_to_extract.write(data)
        
        return file_name

def retrieve_images(file_type="input_images"):
    if file_type=="input_images":
        iamges = INPUT_IMAGES_COL.find(
                                    {},
                                    {"_id":0, "image_name":1} 
                                    )
        return images

    if file_type=="output_images":
        data = OUTPUT_IMAGES_COL.find(
                                    {},
                                    {"_id":0, "image_name":1} 
                                    )
        return images

def select_image(image_name, image_type="input_image"):
    
    if image_type=="input_image": 
        image = INPUT_IMAGES_COL.find(
                                    {"image_name" : image_name},
                                    {"_id":0, "image_name":1, "raw_image":1 }
                                    )
        image_name = image["image_name"] + ".png"
        base64_image_bytes = data["raw_image"].encode("utf-8")
        with open(image_name, "wb") as image_to_extract:
            decoded_image_data = base64.decodebytes(base64_image_bytes)
            image_to_extract.write(decoded_image_data)

        return image_name


    if image_type=="output_image":
        image = OUTPUT_IMAGES_COL.find(
                                    {"image_name" : image_name},
                                    {"_id":0, "image_name":1, "image":1 }
                                    )

        image_name = image["image_name"] + ".png"
        base64_image_bytes = data["image"].encode("utf-8")
        with open(image_name, "wb") as image_to_extract:
            decoded_image_data = base64.decodebytes(base64_image_bytes)
            image_to_extract.write(decoded_image_data)

        return image_name

   


                            


