db.createUser({
  user: 'signatureteam',
  pwd: 'signaturepass',
  roles: [
    {
      role:  'readWrite',
      db: 'DB',
    },
  ],
});