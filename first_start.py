#!/usr/bin/python
#-*- coding: utf-8 -*-

import helpers_db
import sys
import os
import re

def is_absent(col_name):
    col_names = helpers_db.DB.list_collection_names()

    return col_name not in col_names

def import_files_in_db():
    helpers_db.add_file(file ='./model_1.cfg', file_type="config")
    helpers_db.add_file(file='./model_1.weights', file_type="weights")
    helpers_db.add_file(file='./model_2.cfg', file_type="config")
    helpers_db.add_file(file='./model_2.weights', file_type="weights")

def clean_folder():
    pattern = re.compile(r'^.*(\.cfg|\.weights)$')
    for file in os.listdir('/app'):
        if re.search(pattern, file):
            os.remove(file)

if __name__ == "__main__":

   if is_absent(col_name='CONFIG_COL'):
       import_files_in_db()
       clean_folder()
        
        




        


