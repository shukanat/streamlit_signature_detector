# Simple Streamlit app for testing signature detection with Yolo v4
![Yolo](https://www.nakasha.co.jp/img/future/ai/yolov3train/YOLO.jpg)
## Quick start guide
1. Download repository<br/>
`$git clone https://gitlab.com/shukanat/streamlit_signature_detector.git`<br/>
`$cd streamlit_signature_detector`
2. Update database cedentials on docker-compose.yml</br>
``` 
MONGO_INITDB_ROOT_USERNAME: <login>
MONGO_INITDB_ROOT_PASSWORD: <password> 
```
3. Build working enviroment with docker-compose<br/>
`$docker-compose build`

4. Lauch app and database containers<br/>
`$docker-compose up -d`

5. Go inside the app container</br>
`$docker exec -it opencv-streamlit /bin/bash`

6. Add a .env file inside the container and fill the fiels according to your docker-compose.yml</br>
```
DOMAIN = "mongodb://mongodb"
PORT = 27017
USERNAME = <login>
PASSWORD = <password>
```
7. Only for the app container : run *first_start.sh* script to download configuration files and weights and add them to database:<br/>
`$sh first_start.sh`

8. Application will be available at:<br/>
`http://localhost:8501/`
